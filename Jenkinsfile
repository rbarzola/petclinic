def versionapp = "initial"
def artifact_id = "initial"
pipeline {
    agent any
    stages {
        stage('Git Checkout') {
            steps {
                script {
                properties([pipelineTriggers([pollSCM('H * * * *')])])
                git pool: true,credentialsId: 'gitlab-cred', branch: 'developer', url: 'https://gitlab.com/osenterprise/petclinic.git'
                def mavenPom = readMavenPom file: 'pom.xml'
                versionapp = "${mavenPom.version}"
                artifact_id = "${mavenPom.artifactId}"
                }  
            }
        }
        stage('Compile App Maven') {
            steps {
                withMaven(globalMavenSettingsConfig: 'eae34db3-166d-4cfa-8dd9-3482767b10a3', jdk: 'jdk-8', maven: 'maven', mavenSettingsConfig: '83aa8087-234d-4bf7-b532-3a677599dd5e') {
                sh 'mvn package -Dmaven.test.skip'
            }
            }  
        }
        stage('Upload artifact to Nexus') {
            steps {
                script{
                nexusArtifactUploader artifacts: [
                     [
                        artifactId: 'spring-petclinic', 
                        classifier: '', 
                        file: 'target/petclinic.war', 
                        type: 'war'
                    ]
                ], 
                credentialsId: 'nexus-creds', 
                groupId: 'org.springframework.samples', 
                nexusUrl: '192.168.1.104:8081', 
                nexusVersion: 'nexus3', 
                protocol: 'http', 
                repository: 'petclinic-release', 
                version: "${versionapp}"
                }
            }
        }
        stage('Build Container Image') {
            steps {
                script {
                sh "sed -i 's/APP_VERSION/${versionapp}/g' Dockerfile"
                sh "sed -i 's/APP_NAME/${artifact_id}/g' Dockerfile"
                sh "curl http://192.168.1.104:8081/repository/petclinic-release/org/springframework/samples/${artifact_id}/${versionapp}/${artifact_id}-${versionapp}.war -o ${artifact_id}-${versionapp}.war"
                sh "mv ${artifact_id}-${versionapp}.war petclinic.war"
                sh "docker build -t 040500/${artifact_id}:${versionapp} ."
                }
            }
        }
        stage('Push Image to DockerHub') {
            steps {
                script {
                withCredentials([usernamePassword(credentialsId: 'docker-token', passwordVariable: 'DOCKERHUBPASS', usernameVariable: 'DOCKERHUBUSER')]) {
                sh "docker login -u ${DOCKERHUBUSER} -p ${DOCKERHUBPASS}"
                }
                sh "docker push 040500/${artifact_id}:${versionapp}"
                }
            }
        }
        stage('Deploy App - Dev') {
            steps {
                script {
                sh 'git config user.email miguel.vila@osenterprise.com.pe'
                sh 'git config user.name Miguel'
                sh 'git checkout developer'
                sh 'GIT_SSH_COMMAND = "ssh -i /var/jenkins_home/ssh"'
                sh "sed -i 's+040500/spring-petclinic.*+040500/spring-petclinic:${versionapp}+g' deploy/dev/deployment.yaml"
                sh 'git add .'
                sh "git commit -m 'Update image version: ${versionapp}'"
                sh 'git push git@gitlab.com:osenterprise/petclinic.git'
                }
            }
        }
    }
}